[string]$ProGetUri    = 'localhost'
[string]$ProGetApiKey = ''

Get-ChildItem -Path $PSScriptRoot/Functions -Recurse -File `
| ForEach-Object -Process {
    . $PSItem.FullName
}

Export-ModuleMember -Function * -Variable ProGetUri,ProGetApiKey