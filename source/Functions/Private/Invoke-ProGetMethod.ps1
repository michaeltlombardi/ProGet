function Invoke-ProGetMethod {
    [CmdletBinding()]
    param (
        [string]$ApiKey = $ProGetApiKey,
        [string]$Method,
        [string]$Uri = $ProGetUri,
        [hashtable]$Arguments
    )
    
    begin {
        $ApiCallParameters = @{
            Method = 'Get'
            Uri    = "http://$uri/api/json/${Method}?API_Key=${ApiKey}"
        }
    }
    
    process {
        ForEach ($Key in $Arguments.Keys) {
            $ApiCallParameters.Uri += "&${Key}=$($Arguments.$Key)"
        }
        Invoke-WebRequest @ApiCallParameters `
        | Select-Object -ExpandProperty 'Content' `
        | ConvertFrom-Json
    }
    
    end {
    }
}